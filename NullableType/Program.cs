﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NullableType
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene { Nimi = "Henn", Sünniaeg = new DateTime(1955,3,7)};
            Inimene ants = new Inimene { Nimi = "Ants" };

            Inimene malle = new Inimene { Nimi = "Malle", Sünniaeg = new DateTime(1968, 7, 20) };
            Inimene pille = new Inimene { Nimi = "Pille" };

            ants.Abielu(malle);

            foreach (var x in Inimene.Inimesed) Console.WriteLine(x);
                        
        }
    }

    class Inimene
    {
        public static List<Inimene> Inimesed = new List<Inimene>();

        public string Nimi;
        public DateTime? Sünniaeg = null;
        public Inimene Kaasa = null;

        public Inimene()
        {
            Inimesed.Add(this);
        }

        public int? Vanus
        {
            get
            {
                if (Sünniaeg.HasValue)
                {
                    int v = DateTime.Now.Year - Sünniaeg.Value.Year;
                    if (Sünniaeg.Value.AddYears(v) > DateTime.Today) v--;
                    return v;
                }
                else return null;
            }
        }

        public override string ToString()
        {
            return $"inimene {Nimi} vanusega {Vanus ?? -1 } abikaasa {Kaasa?.Nimi ?? "puudub" }";
        }

        public void Abielu(Inimene kaasa)
        {
            this.Kaasa = kaasa;
            kaasa.Kaasa = this;
        }

    }


}
